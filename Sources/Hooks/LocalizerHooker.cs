﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Iodynis.Libraries.Localizing.Internal;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerHooker
    {
        private List<LocalizerHook> Hooks { get; } = new List<LocalizerHook>();
        private Dictionary<string, HashSet<LocalizerHook>> KeyUsage { get; } = new Dictionary<string, HashSet<LocalizerHook>>();

        private bool isSuspended = false;
        private bool isLocalizationRequired = false;

        public event EventHandler<Exception> Error;

        internal Localizer Localizer { get; }
        public LocalizerHooker(Localizer localizer)
        {
            Localizer = localizer;

            Localizer.Settings.LanguageChanged += LanguageChanged;
            Localizer.Cache.ValueChanged += CacheValueChanged;
            Localizer.Storage.ValueChanged += StorageValueChanged;
        }

        private void CacheValueChanged(object sender, LocalizerCacheValueChangedEventArgs arguments)
        {
            Localize(arguments.Key);
        }
        private void StorageValueChanged(object sender, LocalizerStorageValueChangedEventArgs arguments)
        {
            Localize(arguments.Key);
        }

        private void LanguageChanged(object sender, string language)
        {
            Localize();
        }
        /// <summary>
        /// Suspend automatic updates in case of language changes.
        /// </summary>
        public void Suspend()
        {
            isSuspended = true;
            isLocalizationRequired = false;
        }
        /// <summary>
        /// Resume automatic updates in case of language changes.
        /// If language was changed after the suspension then the update will be done immediately.
        /// </summary>
        public void Resume()
        {
            isSuspended = false;
            if (isLocalizationRequired)
            {
                Localize();
                isLocalizationRequired = false;
            }
        }
        /// <summary>
        /// Update only those hooks that depend on the specified key.
        /// </summary>
        /// <param name="key">The key hooks depend on.</param>
        private void Localize(string key)
        {
            if (!KeyUsage.TryGetValue(key, out HashSet<LocalizerHook> hooks))
            {
                return;
            }
            // Key-to-hooks relations are modified during localization because different languages may depend on totally different key chains
            List<LocalizerHook> hooksCopy = new List<LocalizerHook>(hooks);
            foreach (LocalizerHook hook in hooksCopy)
            {
                try
                {
                    hook.Localize(false);
                }
                catch (Exception exception)
                {
                    Error?.Invoke(this, exception);
                }
            }
        }
        /// <summary>
        /// Update all hooks.
        /// </summary>
        public void Localize()
        {
            if (isSuspended)
            {
                isLocalizationRequired = true;
                return;
            }
            for (int hookIndex = 0; hookIndex < Hooks.Count; hookIndex++)
            {
                LocalizerHook hook = Hooks[hookIndex];
                // Remove dead hooks
                if (!hook.IsAlive)
                {
                    Hooks.RemoveAt(hookIndex--);
                    continue;
                }
                hook.Localize();
            }
        }
        //public void Tick(string key)
        //{
        //    if (KeyUsage.TryGetValue(key, out List<LocalizerHook> hooks))
        //    {
        //        foreach (LocalizerHook hook in hooks)
        //        {
        //            hook.Localize();
        //        }
        //    }
        //}
        public LocalizerHookedAction Hook(Func<string, Tuple<string, List<string>>> getter, Action<string> setter)
        {
            LocalizerHookedAction hook = new LocalizerHookedAction(this, getter, setter);
            Hooks.Add(hook);
            return hook;
        }
        /// <summary>
        /// Hook to the property or field of the specified object.
        /// </summary>
        /// <param name="object">Object to hook to.</param>
        /// <param name="propertyOrFieldName">Name of the property or field to hook to.</param>
        /// <returns></returns>
        public LocalizerHook Hook(object @object, string propertyOrFieldName)
        {
            LocalizerHookedObject hook = new LocalizerHookedObject(this, @object, PropertyField.Get(@object, propertyOrFieldName));
            Hooks.Add(hook);
            return hook;
        }
        /// <summary>
        /// Hook to the field of the specified object.
        /// </summary>
        /// <param name="object">Object to hook to.</param>
        /// <param name="fieldInfo">Field information to hook to.</param>
        /// <returns></returns>
        public LocalizerHook Hook(object @object, FieldInfo fieldInfo)
        {
            LocalizerHookedObject hook = new LocalizerHookedObject(this, @object, new Field(fieldInfo));
            Hooks.Add(hook);
            return hook;
        }
        /// <summary>
        /// Hook to the property of the specified object.
        /// </summary>
        /// <param name="object">Object to hook to.</param>
        /// <param name="propertyInfo">Property information to hook to.</param>
        /// <returns></returns>
        public LocalizerHook Hook(object @object, PropertyInfo propertyInfo)
        {
            LocalizerHookedObject hook = new LocalizerHookedObject(this, @object, new Property(propertyInfo));
            Hooks.Add(hook);
            return hook;
        }
        ///// <summary>
        ///// Unhook and hook again to the property or field of the specified object.
        ///// </summary>
        ///// <param name="object">Object to rehook.</param>
        ///// <param name="propertyOrFieldName">Name of the property or field to rehook.</param>
        ///// <returns></returns>
        //public LocalizerHook Rehook(object @object, string propertyOrFieldName)
        //{
        //    Unhook(@object, propertyOrFieldName);
        //    return Hook(@object, propertyOrFieldName);
        //}
        /// <summary>
        /// Unhook the specified hook.
        /// </summary>
        /// <param name="hook">The hook to unhook.</param>
        public void Unhook(LocalizerHook hook)
        {
            FreeKeys(hook);
            Hooks.Remove(hook);
        }
        //public void Unhook(object @object)
        //{
        //    for (int i = 0; i < Hooks.Count; i++)
        //    {
        //        if (Hooks[i].Is(@object))
        //        {
        //            FreeKeys(Hooks[i]);
        //            Hooks.RemoveAt(i--);
        //        }
        //    }
        //}
        //public void Unhook(object @object, string propertyOrFieldName)
        //{
        //    for (int i = 0; i < Hooks.Count; i++)
        //    {
        //        if (Hooks[i].Is(@object, propertyOrFieldName))
        //        {
        //            FreeKeys(Hooks[i]);
        //            Hooks.RemoveAt(i);
        //            break;
        //        }
        //    }
        //}
        internal void UseKeys(LocalizerHook hook)
        {
            foreach (string key in hook.Keys)
            {
                if (!KeyUsage.TryGetValue(key, out HashSet<LocalizerHook> hooks))
                {
                    hooks = new HashSet<LocalizerHook>();
                    KeyUsage.Add(key, hooks);
                }
                hooks.Add(hook);
            }
        }
        internal void FreeKeys(LocalizerHook hook)
        {
            foreach (string key in hook.Keys)
            {
                if (KeyUsage.TryGetValue(key, out HashSet<LocalizerHook> hooks))
                {
                    hooks.Remove(hook);
                    if (hooks.Count == 0)
                    {
                        KeyUsage.Remove(key);
                    }
                }
            }
        }
    }
}
