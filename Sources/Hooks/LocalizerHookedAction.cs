﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Localizing.Internal
{
    public class LocalizerHookedAction : LocalizerHook
    {
        public override bool IsAlive => true;

        public Func<string, Tuple<string, List<string>>> Getter { get; }
        public Action<string> Setter { get; }

        public LocalizerHookedAction(LocalizerHooker hooker, Func<string, Tuple<string, List<string>>> getter, Action<string> setter)
            : base(hooker)
        {
            Getter = getter;
            Setter = setter;

            // Set used keys
            Tuple<string, List<string>> stringAndKeys = Getter(Localizer.Settings.Language);
            Keys = stringAndKeys.Item2;
            Hooker.UseKeys(this);
        }

        protected override void Localize(bool rebuildKeys, string language, params object[] values)
        {
            // Get
            Tuple<string, List<string>> stringAndKeys = Getter(Localizer.Settings.Language);
            string @string = stringAndKeys.Item1;
            List<string> keys = stringAndKeys.Item2;

            // Set
            Setter(@string);

            // Hook onto replaced keys, so the hooker will automatically call localize again if they are changed
            if (rebuildKeys)
            {
                Hooker.FreeKeys(this);
                Keys = keys;
                Hooker.UseKeys(this);
            }
        }
        protected override void Delocalize()
        {
            if (Keys != null)
            {
                Hooker.UseKeys(this);
            }
            Keys = null;
        }
    }
}
