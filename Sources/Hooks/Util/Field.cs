﻿using System;
using System.Reflection;

namespace Iodynis.Libraries.Localizing.Internal
{
    public class Field : PropertyField
    {
        public override Type Type => FieldInfo.FieldType;
        public override string Name => FieldInfo.Name;
        public FieldInfo FieldInfo { get; }
        public Field(object @object, string fieldName)
        {
            FieldInfo = @object.GetType().GetField(fieldName);
        }
        public Field(FieldInfo fieldInfo)
        {
            FieldInfo = fieldInfo;
        }
        public override object GetValue(object @object)
        {
            return FieldInfo.GetValue(@object);
        }
        public override void SetValue(object @object, object value)
        {
            FieldInfo.SetValue(@object, value);
        }
    }
}
