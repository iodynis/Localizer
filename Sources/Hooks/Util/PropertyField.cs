﻿using System;
using System.Reflection;

namespace Iodynis.Libraries.Localizing.Internal
{
    public abstract class PropertyField
    {
        public virtual Type Type
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public virtual string Name
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public PropertyField()
        {
            ;
        }
        public static PropertyField Get(object @object, string propertyOrFieldName)
        {
            Type type = @object.GetType();
            FieldInfo fieldInfo = type.GetField(propertyOrFieldName);
            if (fieldInfo != null)
            {
                return new Field(fieldInfo);
            }
            PropertyInfo propertyInfo = type.GetProperty(propertyOrFieldName);
            if (propertyInfo != null)
            {
                return new Property(propertyInfo);
            }
            throw new Exception($"Type {type} does not have neither field nor property named {propertyOrFieldName}.");
        }
        public virtual object GetValue(object @object)
        {
            throw new NotImplementedException();
        }
        public virtual object GetValue(object @object, object[] indexes)
        {
            throw new NotImplementedException();
        }
        public virtual void SetValue(object @object, object value)
        {
            throw new NotImplementedException();
        }
        public virtual void SetValue(object @object, object value, object[] indexes)
        {
            throw new NotImplementedException();
        }
    }
}
