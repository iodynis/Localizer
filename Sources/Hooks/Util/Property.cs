﻿using System;
using System.Reflection;

namespace Iodynis.Libraries.Localizing.Internal
{
    public class Property : PropertyField
    {
        public override Type Type => PropertyInfo.PropertyType;
        public override string Name => PropertyInfo.Name;
        public PropertyInfo PropertyInfo { get; }
        public Property(object @object, string fieldName)
        {
            PropertyInfo = @object.GetType().GetProperty(fieldName);
        }
        public Property(PropertyInfo propertyInfo)
        {
            PropertyInfo = propertyInfo;
        }
        public override object GetValue(object @object)
        {
            return PropertyInfo.GetValue(@object);
        }
        public override object GetValue(object @object, object[] indexes)
        {
            return PropertyInfo.GetValue(@object, indexes);
        }
        public override void SetValue(object @object, object value)
        {
            PropertyInfo.SetValue(@object, value);
        }
        public override void SetValue(object @object, object value, object[] indexes)
        {
            PropertyInfo.SetValue(@object, value, indexes);
        }
    }
}
