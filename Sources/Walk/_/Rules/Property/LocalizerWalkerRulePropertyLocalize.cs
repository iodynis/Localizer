﻿//using System;
//using System.Reflection;
//using Iodynis.Libraries.Localizing.Internal;

//namespace Iodynis.Libraries.Localizing
//{
//    public class LocalizerWalkerRulePropertyLocalize : LocalizerWalkerRule
//    {
//        private PropertyInfo PropertyInfo;
//        public LocalizerWalkerRulePropertyLocalize(PropertyInfo propertyInfo)
//        {
//            PropertyInfo = propertyInfo;
//        }
//        public LocalizerWalkerRulePropertyLocalize(Type type, string name)
//        {
//            PropertyInfo = type.GetProperty(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
//        }
//        public override bool Match(Type type)
//        {
//            return PropertyInfo.DeclaringType == type;
//        }
//        public override void Apply(LocalizerWalkerTypeInfo info)
//        {
//            info.LocalizeProperties.Add(PropertyInfo);
//        }
//    }

//    public static partial class Extensions
//    {
//        public static void Localize(this LocalizerWalker walker, PropertyInfo propertyInfo)
//        {
//            walker.RuleAdd(new LocalizerWalkerRulePropertyLocalize(propertyInfo));
//        }
//        public static void Localize(this LocalizerWalker walker, Type type, string name)
//        {
//            walker.RuleAdd(new LocalizerWalkerRulePropertyLocalize(type, name));
//        }
//    }
//}
