﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iodynis.Libraries.Localizing.Internal
{
    public class LocalizerWalkerTypeInfo
    {
        public Type Type;

        public List<PropertyInfo> LocalizeProperties = new List<PropertyInfo>();
        public List<FieldInfo>    LocalizeFields     = new List<FieldInfo>();
        public List<PropertyInfo> WalkProperties = new List<PropertyInfo>();
        public List<FieldInfo>    WalkFields     = new List<FieldInfo>();
        public List<Func<object, object>>              RuntimeObjects     = new List<Func<object, object>>();
        public List<Func<object, IEnumerable<object>>> RuntimeEnumerables = new List<Func<object, IEnumerable<object>>>();

        public LocalizerWalkerTypeInfo(Type type)
        {
            Type = type;
        }
    }
}
