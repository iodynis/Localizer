﻿using System;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerCacheValueChangedEventArgs : EventArgs
    {
        public string Key;
        public object[] Values;
        public LocalizerCacheValueChangedEventArgs(string key, object[] values)
        {
            Key = key;
            Values = values;
        }
    }
}
