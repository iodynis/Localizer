﻿namespace Iodynis.Libraries.Localizing.Internal
{
    internal enum LocalizerParserSyntaxerToken : int
    {
        FUNCTION,
        ARGUMENT,
        FUNCTION_ARGUMENT,

        NOT,
        AND,
        OR,
        XOR,
        ASSIGN,
        EQUAL,
        NOT_EQUAL,
        LESS,
        GREATER,
        LESS_OR_EQUAL,
        GREATER_OR_EQUAL,
        MULTIPLY,
        DIVIDE,
        REMINDER,
        PLUS,
        DOUBLEPLUS,
        PREPLUS,
        POSTPLUS,
        OPERATOR_NOT,
        MINUS,
        DOUBLEMINUS,
        PREMINUS,
        POSTMINUS,
    }
}
