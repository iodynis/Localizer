﻿namespace Iodynis.Libraries.Localizing
{
    public class LocalizerDefaults
    {
        /// <summary>
        /// Enumeration function.
        /// Usage: {0:enumeration(1,2,3)}.
        /// </summary>
        public LocalizerFunctionEnumeration FunctionEnumeration { get; internal set; }
        /// <summary>
        /// Lowercase function.
        /// Usage: {0:lowercase()}.
        /// </summary>
        public LocalizerFunctionLowerCase FunctionLowerCase { get; internal set; }
        /// <summary>
        /// Uppercase function.
        /// Usage: {0:uppercase()}.
        /// </summary>
        public LocalizerFunctionUpperCase FunctionUpperCase { get; internal set; }
        /// <summary>
        /// Language function.
        /// Usage: {0:language(en-GB)}.
        /// </summary>
        public LocalizerFunctionLanguage FunctionLanguage { get; internal set; }
        /// <summary>
        /// Plural function.
        /// Usage: {0:plural(english,,s)} or {0:plural(#,,s)}.
        /// </summary>
        public LocalizerFunctionPlural FunctionPlural { get; internal set; }
        /// <summary>
        /// Sex function. It is actually an enumeration.
        /// Usage: {0:sex(female,male,it,females,males,its)}, {0:sex(female,male,it)} or {0:sex(female,male)}.
        /// </summary>
        public LocalizerFunctionEnumeration FunctionSex { get; internal set; }

        internal Localizer Localizer { get; }
        internal LocalizerDefaults(Localizer localizer)
        {
            Localizer = localizer;
        }
    }
}
