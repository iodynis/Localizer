﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerFunctionPlural : LocalizerFunction
    {
        private static readonly Dictionary<int, Func<string, string[], string>> PluralVariantsCustom = new Dictionary<int, Func<string, string[], string>>();
        /// <summary>
        /// Depenging on the value according to the provided language or plural form index in the first argument selects the appropriate plural form from following arguments.
        /// Available languages and their plural form index according to https://developer.mozilla.org/en-US/docs/Mozilla/Localization/Localization_and_Plurals are:
        /// 0: Chinese, Japanese, Korean, Persian, Turkic, Altaic, Thai, Lao;
        /// 1: Danish, Dutch, English, Faroese, Frisian, German, Norwegian, Swedish, Estonian, Finnish, Hungarian, Basque, Greek, Hebrew, Italian, Portuguese, Spanish, Catalan, Vietnamese;
        /// 2: French, Brazilian portuguese;
        /// 3: Latvian, Latgalian;
        /// 4: Scottish gaelic;
        /// 5: Romanian;
        /// 6: Lithuanian;
        /// 7: Belarusian, Bosnian, Croatian, Serbian, Russian, Ukrainian;
        /// 8: Slovak, Czech;
        /// 9: Polish;
        /// 10: Slovenian, Sorbian;
        /// 11: Irish gaelic;
        /// 12: Arabic;
        /// 13: Maltese;
        /// 14 is unused;
        /// 15: Icelandic, Macedonian;
        /// 16: Breton;
        /// 17: Shuar;
        /// 18: Welsh.
        ///
        /// Usage examples:
        /// "We need {0} cand{0:plural(#,y,ies)} to live the day",
        /// "We need {0} cand{0:plural(1,y,ies)} to live the day",
        /// "We need {0} cand{0:plural(english,y,ies)} to live the day",
        /// "Нам нужно {0} конф{0:plural(7,ета,ы,)}, чтобы прожить этот день",
        /// "Нам нужно {0} конф{0:plural(russian,ета,ы,)}, чтобы прожить этот день".
        /// </summary>
        public readonly Dictionary<string, int> PluralVariantsByLanguage = new Dictionary<string, int>()
        {
            // TODO: add languages in xx and xx-XX forms.
            { "chinese", 0 },
            { "japanese", 0 },
            { "korean", 0 },
            { "persian", 0 },
            { "turkic", 0 },
            { "altaic", 0 },
            { "thai", 0 },
            { "lao", 0 },
            { "danish", 1 },
            { "dutch", 1 },
            { "english", 1 },
            { "faroese", 1 },
            { "frisian", 1 },
            { "german", 1 },
            { "norwegian", 1 },
            { "swedish", 1 },
            { "estonian", 1 },
            { "finnish", 1 },
            { "hungarian", 1 },
            { "basque", 1 },
            { "greek", 1 },
            { "hebrew", 1 },
            { "italian", 1 },
            { "portuguese", 1 },
            { "spanish", 1 },
            { "catalan", 1 },
            { "vietnamese", 1 },
            { "french", 2 },
            { "brazilian portuguese", 2 },
            { "latvian", 3 },
            { "latgalian", 3 },
            { "scottish gaelic", 4 },
            { "romanian", 5 },
            { "lithuanian", 6 },
            { "belarusian", 7 },
            { "bosnian", 7 },
            { "croatian", 7 },
            { "serbian", 7 },
            { "russian", 7 },
            { "ukrainian", 7 },
            { "slovak", 8 },
            { "czech", 8 },
            { "polish", 9 },
            { "slovenian", 10 },
            { "sorbian", 10 },
            { "irish gaelic", 11 },
            { "arabic", 12 },
            { "maltese", 13 },
            /* 14 is unused */
            { "icelandic", 15 },
            { "macedonian", 15 },
            { "breton", 16 },
            { "shuar", 17 },
            { "welsh", 18 },

            { "en", 1 },    // English
            { "en-GB", 1 }, // English (Graet Britain)
            { "en-US", 1 }, // English (United States of America)
            { "es", 1 },    // Spanish
            { "es-ES", 1 }, // Spanis (Spain)
            { "de", 1 },    // German
            { "de-DE", 1 }, // German (Germany)
            { "ja", 0 },    // Japanese
            { "ja-JP", 0 }, // Japanese (Japan)
            { "ko", 0 },    // Korean
            { "ko-KR", 0 }, // Korean (Korea)
            { "it", 1 },    // Italian
            { "it-IT", 1 }, // Italian (Italy)
            { "ru", 7 },    // Russian
            { "ru-RU", 7 }, // Russian (Russia)
            { "zh", 0 },    // Chinese
            { "zh-CH", 0 }, // Chinese (Simplified)
            { "zh-HK", 0 }, // Chinese (Hong Kong)
            { "zh-MO", 0 }, // Chinese (Macau)
            { "zh-SG", 0 }, // Chinese (Singapore)
            { "zh-TW", 0 }, // Chinese (Traditional)
        };
        //public IReadOnlyDictionary<string, int> PluralVariantsByLanguage
        //{
        //    get
        //    {
        //        return PluralVariantsByLanguage;
        //    }
        //}

        /// <summary>
        /// Pluralize depending on count and language provided.
        /// </summary>
        /// <param name="name">Name of the function.</param>
        public LocalizerFunctionPlural(string name = "Plural")
            : base(name)
        {
            ;
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="language">Lanaguge based on which the correct variant will be chosen</param>
        /// <param name="arguments">Plural rule index first, then all variants. For more information about plural rule index visit for more info visit https://developer.mozilla.org/en-US/docs/Mozilla/Localization/Localization_and_Plurals .</param>
        /// <returns></returns>
        public override object Invoke(string language, params object[] arguments)
        {
            if (arguments.Length == 0)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) does not support 0 arguments.");
            }
            if (arguments.Length == 1)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) does not support 1 argument.");
            }

            if (arguments[0] == null)
            {
                return null;
            }

            // Count is not converted to integer because it can be too long.
            // Instead, only its length and the last one or two digits are analyzed.
            string count = arguments[0].ToString();
            string pluralVariant = arguments[1]?.ToString();
            int pluralVariantIndex = -1;

            // If current default language
            if (pluralVariant == null)
            {
                if (!PluralVariantsByLanguage.TryGetValue(language, out pluralVariantIndex))
                {
                    throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for value {arguments[1]}. Language {language} was not found in the plural forms ist.");
                }
            }
            else if (PluralVariantsByLanguage.TryGetValue(pluralVariant, out pluralVariantIndex))
            {
                // Found the language
            }
            else
            {
                try
                {
                    pluralVariantIndex = Convert.ToInt32(arguments[1]);
                }
                catch (Exception exception)
                {
                    throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for value {arguments[1]}. Variant {arguments[1]} is neither a number nor it could be found in the plural forms ist.", exception);
                }
            }
            // Check that a correct plural form index was provided
            if (pluralVariantIndex < 0)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for value {arguments[1]}. Variant {arguments[1]} is negative.");
            }
            if (pluralVariantIndex >= PluralVariantsByLanguage.Count)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for value {arguments[1]}. Variant {arguments[1]} is greater or equal to the count of the plural forms available ({PluralVariantsByLanguage.Count} in total).");
            }

            string[] pluralVariants = new string[arguments.Length - 2];
            for (int i = 0; i < pluralVariants.Length; i++)
            {
                pluralVariants[i] = arguments[i + 2].ToString();
            }

            // Only last two digits actually used to determine the plural variant
            int lastTwoDigits;
            int lastOneDigit;

            switch (pluralVariantIndex)
            {
                // Asian (Chinese, Japanese, Korean), Persian, Turkic/Altaic (Turkish), Thai, Lao
                //
                // Everything: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, …
                //
                case 0:
                    if (arguments.Length != 3)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 3 arguments.");
                    }
                    return pluralVariants[0];

                // Germanic (Danish, Dutch, English, Faroese, Frisian, German, Norwegian, Swedish), Finno-Ugric (Estonian, Finnish, Hungarian), Language isolate (Basque), Latin/Greek (Greek), Semitic (Hebrew), Romanic (Italian, Portuguese, Spanish, Catalan), Vietnamese
                //
                // Is 1:            1
                // Everything else: 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, …
                //
                case 1:
                    if (arguments.Length != 4)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 4 arguments.");
                    }
                    if (count == "1") return pluralVariants[0];
                    return pluralVariants[1];

                // Romanic (French, Brazilian Portuguese)
                //
                // Is 0 or 1:       0, 1
                // Everything else: 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, …
                //
                case 2:
                    if (arguments.Length != 4)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 4 arguments.");
                    }
                    if (count == "0" || count == "1") return pluralVariants[0];
                    return pluralVariants[1];

                // Baltic (Latvian, Latgalian)
                //
                // Ends in 0:               0
                // Ends in 1, excluding 11: 1, 21, 31, 41, 51, 61, 71, 81, 91, 101, 121, 131, 141, 151, 161, 171, 181, 191, 201, 221, 231, 241, 251, 261, 271, 281, 291, …
                // Everything else:         2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 22, 23, 24, 25, 26, 27, 28, 29, 32, 33, 34, 35, 36, 37, 38, 39, 42, 43, 44, 45, 46, 47, 48, 49, 52, 53, …
                //
                case 3:
                    if (arguments.Length != 5)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 5 arguments.");
                    }
                    lastOneDigit = GetLastOneDigit(count);
                    if (lastOneDigit == 0) return pluralVariants[0];
                    if (lastOneDigit == 1 && count != "11") return pluralVariants[1];
                    return  pluralVariants[2];

                // Celtic (Scottish Gaelic)
                //
                // Is 1 or 11:       1, 11
                // Is 2 or 12:       2, 12
                // Is 3-10 or 13-19: 3, 4, 5, 6, 7, 8, 9, 10, 13, 14, 15, 16, 17, 18, 19
                // Everything else:  0, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, …
                //
                case 4:
                    if (arguments.Length != 6)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 6 arguments.");
                    }
                    if (count == "1" || count == "11") return pluralVariants[0];
                    if (count == "2" || count == "12") return pluralVariants[1];
                    lastTwoDigits = GetLastTwoDigits(count);
                    if (count.Length <= 2 /* is a 1- or 2-digit number, and therefore it is equal to lastTwoDigits */ &&
                        ((3 <= lastTwoDigits && lastTwoDigits <= 10) /* is 3-10 */ || (13 <= lastTwoDigits && lastTwoDigits <= 19) /* or 13-19 */)) return pluralVariants[2];
                    return pluralVariants[3];

                // Romanic (Romanian)
                //
                // Is 1:                               1
                // Is 0 or ends in 01-19, excluding 1: 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, …
                // Everything else:                    20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, …
                //
                case 5:
                    if (arguments.Length != 5)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 5 arguments.");
                    }
                    if (count == "1") return pluralVariants[0];
                    lastTwoDigits = GetLastTwoDigits(count);
                    if (count == "0" || (1 <= lastTwoDigits && lastTwoDigits <= 19) /* ends in 01-19 */) return pluralVariants[1];
                    return pluralVariants[2];

                // Baltic (Lithuanian)
                //
                // Ends in 1, excluding 11:    1, 21, 31, 41, 51, 61, 71, 81, 91, 101, 121, 131, 141, 151, 161, 171, 181, 191, 201, 221, 231, 241, 251, 261, 271, 281, 291, …
                // Ends in 0 or ends in 11-19: 0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, …
                // Everything else:            2, 3, 4, 5, 6, 7, 8, 9, 22, 23, 24, 25, 26, 27, 28, 29, 32, 33, 34, 35, 36, 37, 38, 39, 42, 43, 44, 45, 46, 47, 48, 49, 52, 53, 54, 55, 56, 57, 58, 59, 62, 63, 64, 65, 66, 67, 68, 69, 72, 73, …
                //
                case 6:
                    if (arguments.Length != 5)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 5 arguments.");
                    }
                    lastTwoDigits = GetLastTwoDigits(count);
                    lastOneDigit = lastTwoDigits % 10;
                    if (lastOneDigit == 1 && count != "11") return pluralVariants[0];
                    if (lastOneDigit == 0 || (11 <= lastTwoDigits && lastTwoDigits <= 19) /* ends in 11-19 */) return pluralVariants[1];
                    return pluralVariants[2];

                // Slavic (Belarusian, Bosnian, Croatian, Serbian, Russian, Ukrainian)
                //
                // Ends in 1, excluding 11:      1, 21, 31, 41, 51, 61, 71, 81, 91, 101, 121, 131, 141, 151, 161, 171, 181, 191, 201, 221, 231, 241, 251, 261, 271, 281, 291, …
                // Ends in 2-4, excluding 12-14: 2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54, 62, 63, 64, 72, 73, 74, 82, 83, 84, 92, 93, 94, 102, 103, 104, 122, 123, 124, 132, 133, 134, 142, 143, 144, 152, 153, 154, 162, 163, 164, 172, 173, 174, 182, 183, …
                // Everything else:              0, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 26, 27, 28, 29, 30, 35, 36, 37, 38, 39, 40, 45, 46, 47, 48, 49, 50, 55, 56, 57, 58, 59, 60, 65, 66, 67, 68, 69, 70, 75, 76, 77, …, 112, 113, ..., 212, 213, ...
                //
                case 7:
                    if (arguments.Length != 5)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 5 arguments.");
                    }
                    lastTwoDigits = GetLastTwoDigits(count);
                    lastOneDigit = lastTwoDigits % 10;
                    if (lastOneDigit == 1 && count != "11") return pluralVariants[0];
                    if ((2 <= lastOneDigit && lastOneDigit <= 4) /* ends in 2-4 */ &&
                        !(count.Length <= 2 /* is a 1- or 2-digit number, and therefore it is equal to lastTwoDigits */ && 12 <= lastTwoDigits && lastTwoDigits <= 14) /* excluding 12-14 */) return pluralVariants[1];
                    return pluralVariants[2];

                // Slavic (Slovak, Czech)
                //
                // Is 1:            1
                // Is 2-4:          2, 3, 4
                // Everything else: 0, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, …
                //
                case 8:
                    if (arguments.Length != 5)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 5 arguments.");
                    }
                    if (count == "1") return pluralVariants[0];
                    lastOneDigit = GetLastOneDigit(count);
                    if (count.Length <= 1 /* is a 1-digit number, and therefore it is equal to lastOneDigit */ && 2 <= lastOneDigit && lastOneDigit <= 4 /* 2-4 */) return pluralVariants[1];
                    return pluralVariants[2];

                // Slavic (Polish)
                //
                // Is 1:                         1
                // Ends in 2-4, excluding 12-14: 2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54, 62, 63, 64, 72, 73, 74, 82, 83, 84, 92, 93, 94, 102, 103, 104, 122, 123, 124, 132, 133, 134, 142, 143, 144, 152, 153, 154, 162, 163, 164, 172, 173, 174, 182, 183, …
                // Everything else:              0, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 25, 26, 27, 28, 29, 30, 31, 35, 36, 37, 38, 39, 40, 41, 45, 46, 47, 48, 49, 50, 51, 55, 56, 57, 58, 59, 60, 61, 65, 66, 67, 68, …
                //
                case 9:
                    if (arguments.Length != 5)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 5 arguments.");
                    }
                    if (count == "1") return pluralVariants[0];
                    lastTwoDigits = GetLastTwoDigits(count);
                    lastOneDigit = lastTwoDigits % 10;
                    if ((2 <= lastOneDigit && lastOneDigit <= 4) /* ends in 2-4 */ &&
                        !(count.Length <= 2 /* is a 1- or 2-digit number, and therefore it is equal to lastTwoDigits */ && 12 <= lastTwoDigits && lastTwoDigits <= 14) /* excluding 12-14 */) return pluralVariants[1];
                    return pluralVariants[2];

                // Slavic (Slovenian, Sorbian)
                //
                // Ends in 01:      1, 101, 201, …
                // Ends in 02:      2, 102, 202, …
                // Ends in 03-04:   3, 4, 103, 104, 203, 204, …
                // Everything else: 0, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, …
                //
                case 10:
                    if (arguments.Length != 6)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 6 arguments.");
                    }
                    lastTwoDigits = GetLastTwoDigits(count);
                    if (lastTwoDigits == 1 /* ends in 01 */) return pluralVariants[0];
                    if (lastTwoDigits == 2 /* ends in 02 */) return pluralVariants[1];
                    if (lastTwoDigits == 3 || lastTwoDigits == 4 /* ends in 03-04 */) return pluralVariants[2];
                    return pluralVariants[3];

                // Celtic (Irish Gaelic)
                //
                // Is 1: 1
                // Is 2: 2
                // Is 3-6: 3, 4, 5, 6
                // Is 7-10: 7, 8, 9, 10
                // Everything else: 0, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, …
                //
                case 11:
                    if (arguments.Length != 7)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 7 arguments.");
                    }
                    if (count == "1") return pluralVariants[0];
                    if (count == "2") return pluralVariants[1];
                    lastTwoDigits = GetLastTwoDigits(count);
                    lastOneDigit = lastTwoDigits % 10;
                    if (count.Length <= 1 /* is a 1-digit number,       and therefore it is equal to lastOneDigit  */ && 3 <= lastOneDigit  && lastOneDigit  <= 6  /* is 3-6  */) return pluralVariants[2];
                    if (count.Length <= 2 /* is a 1- or 2-digit number, and therefore it is equal to lastTwoDigits */ && 7 <= lastTwoDigits && lastTwoDigits <= 10 /* is 7-10 */) return pluralVariants[3];
                    return pluralVariants[4];

                // Semitic (Arabic)
                //
                // Is 1:                                                      1
                // Is 2:                                                      2
                // Ends in 03-10:                                             3, 4, 5, 6, 7, 8, 9, 10, 103, 104, 105, 106, 107, 108, 109, 110, 203, 204, 205, 206, 207, 208, 209, 210, …
                // Everything else but is 0 and ends in 00-02, excluding 0-2: 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, …
                // Ends in 00-02, excluding 0-2:                              100, 101, 102, 200, 201, 202, …
                // Is 0:                                                      0
                //
                case 12:
                    if (arguments.Length != 8)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 8 arguments.");
                    }
                    if (count == "0") return pluralVariants[5];
                    if (count == "1") return pluralVariants[0];
                    if (count == "2") return pluralVariants[1];
                    lastTwoDigits = GetLastTwoDigits(count);
                    if (0 <= lastTwoDigits && lastTwoDigits <= 2  /* ends in 00-02 */) return pluralVariants[4];
                    if (3 <= lastTwoDigits && lastTwoDigits <= 10 /* ends in 03-10 */) return pluralVariants[2];
                    return pluralVariants[3];

                // Semitic (Maltese)
                //
                // Is 1:                               1
                // Is 0 or ends in 01-10, excluding 1: 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, …
                // Ends in 11-19:                      11, 12, 13, 14, 15, 16, 17, 18, 19, 111, 112, 113, 114, 115, 116, 117, 118, 119, 211, 212, 213, 214, 215, 216, 217, 218, 219, …
                // Everything else:                    20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, …
                //
                case 13:
                    if (arguments.Length != 6)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 6 arguments.");
                    }
                    if (count == "1") return pluralVariants[0];
                    lastTwoDigits = GetLastTwoDigits(count);
                    if (count == "0" || (1 <= lastTwoDigits && lastTwoDigits <= 10) /* ends in 1-10 */) return pluralVariants[1];
                    if (11 <= lastTwoDigits && lastTwoDigits <= 19  /* ends in 11-19 */) return pluralVariants[2];
                    return pluralVariants[3];

                // Unused
                //
                // Ends in 1:       1, 11, 21, 31, 41, 51, 61, 71, 81, 91, 101, 111, 121, 131, 141, 151, 161, 171, 181, 191, 201, 211, 221, 231, 241, 251, 261, 271, 281, 291, …
                // Ends in 2:       2, 12, 22, 32, 42, 52, 62, 72, 82, 92, 102, 112, 122, 132, 142, 152, 162, 172, 182, 192, 202, 212, 222, 232, 242, 252, 262, 272, 282, 292, …
                // Everything else: 0, 3, 4, 5, 6, 7, 8, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 23, 24, 25, 26, 27, 28, 29, 30, 33, 34, 35, 36, 37, 38, 39, 40, 43, 44, 45, 46, 47, 48, 49, 50, 53, 54, 55, 56, 57, 58, 59, 60, 63, …
                //
                case 14:
                    if (arguments.Length != 5)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 5 arguments.");
                    }
                    lastOneDigit = GetLastOneDigit(count);
                    if (lastOneDigit == 1 /* ends in 1 */) return pluralVariants[0];
                    if (lastOneDigit == 2 /* ends in 2 */) return pluralVariants[1];
                    return pluralVariants[2];

                // Icelandic, Macedonian
                //
                // Ends in 1, excluding 11: 1, 21, 31, 41, 51, 61, 71, 81, 91, 101, 121, 131, 141, 151, 161, 171, 181, 191, 201, 221, 231, 241, 251, 261, 271, 281, 291, …
                // Everything else:         0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 52, 53, 54, …
                //
                case 15:
                    if (arguments.Length != 4)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 4 arguments.");
                    }
                    lastOneDigit = GetLastOneDigit(count);
                    if (lastOneDigit == 1 /* ends in 1 */ && count != "11" /* excluding 11 */) return pluralVariants[0];
                    return pluralVariants[1];

                // Celtic (Breton)
                //
                // Ends in 1, excluding 11, 71, 91:                                1, 21, 31, 41, 51, 61, 81, 101, 121, 131, 141, 151, 161, 181, 201, 221, 231, 241, 251, 261, 281, ...
                // Ends in 2, excluding 12, 72, 92:                                2, 22, 32, 42, 52, 62, 82, 102, 122, 132, 142, 152, 162, 182, 202, 222, 232, 242, 252, 262, 282, ...
                // Ends in 3, 4 or 9 excluding 13, 14, 19, 73, 74, 79, 93, 94, 99: 3, 4, 9, 23, 24, 29, 33, 34, 39, 43, 44, 49, 53, 54, 59, ...
                // Ends in 1000000:                                                1000000, 2000000, 3000000, 4000000, 5000000, 6000000, 7000000, 8000000, 9000000, 10000000, ...
                // Everything else:                                                0, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 26, 27, 28, 30, 35, 36, 37, 38, 40, ...
                //
                case 16:
                    if (arguments.Length != 7)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 7 arguments.");
                    }
                    lastTwoDigits = GetLastTwoDigits(count);
                    lastOneDigit = lastTwoDigits % 10;
                    if (lastOneDigit == 1 /* ends in 1 */ && (count.Length != 2 || (lastTwoDigits != 11 && lastTwoDigits != 71 && lastTwoDigits != 91)) /* is not 11, 71 or 91 */) return pluralVariants[0];
                    if (lastOneDigit == 2 /* ends in 2 */ && (count.Length != 2 || (lastTwoDigits != 12 && lastTwoDigits != 72 && lastTwoDigits != 92)) /* is not 12, 72 or 92 */) return pluralVariants[1];
                    if ((lastOneDigit == 3 || lastOneDigit == 4 || lastOneDigit == 9) /* ends in 3, 4 or 9 */ &&
                        (count.Length != 2 || (lastTwoDigits != 13 && lastTwoDigits != 14 && lastTwoDigits != 19 && lastTwoDigits != 73 &&
                        lastTwoDigits != 74 && lastTwoDigits != 79 && lastTwoDigits != 93 && lastTwoDigits != 94 && lastTwoDigits != 99)) /* is not 13, 14, 19, 73, 74, 79, 93, 94 or 99 */) return pluralVariants[2];
                    if (count.EndsWith("000000") /* ends in 000000 */) return pluralVariants[3];
                    return pluralVariants[4];

                // Ecuador indigenous languages (Shuar)
                //
                // Is 0:            0
                // Everything else: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, …
                //
                case 17:
                    if (arguments.Length != 4)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 4 arguments.");
                    }
                    if (count == "0") return pluralVariants[0];
                    return pluralVariants[1];

                // Welsh
                //
                // Is 0:            0
                // Is 1:            1
                // Is 2:            2
                // Is 3:            3
                // Is 6:            6
                // Everything else: 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, …
                //
                case 18:
                    if (arguments.Length != 8)
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. For variant {pluralVariantIndex} it needs exacly 8 arguments.");
                    }
                    if (count == "0") return pluralVariants[0];
                    if (count == "1") return pluralVariants[1];
                    if (count == "2") return pluralVariants[2];
                    if (count == "3") return pluralVariants[3];
                    if (count == "6") return pluralVariants[4];
                    return pluralVariants[5];

                default:
                    // Custom user-defined logic
                    if (PluralVariantsCustom.ContainsKey(pluralVariantIndex))
                    {
                        return PluralVariantsCustom[pluralVariantIndex].Invoke(count, pluralVariants);
                    }
                    else
                    {
                        throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. Variant of {pluralVariantIndex} is not supported.");
                    }
            }
        }
        /// <summary>
        /// Add a custom plural rule.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="function">Function that returns the correct pluarl form based on the count and the plural form list.</param>
        public void Add(int index, Func<string, string[], string> function)
        {
            if (0 <= index || index <= 18)
            {
                throw new Exception($"Cannot add custom plural form with index {index} because the index cannot belong to the interval [0, 18] as indexes for the default plural forms lie there.");
            }
            if (PluralVariantsCustom.ContainsKey(index))
            {
                throw new Exception($"Cannot add custom plural form with index {index} because the index is already used.");
            }
            PluralVariantsCustom.Add(index, function);
        }
        /// <summary>
        /// Remove a plural rule.
        /// </summary>
        /// <param name="index"></param>
        public void Remove(int index)
        {
            if (!PluralVariantsCustom.ContainsKey(index))
            {
                throw new Exception($"Cannot remove custom plural form with index {index} because the index is not used.");
            }
            PluralVariantsCustom.Remove(index);
        }
        /// <summary>
        /// Check if the specified plural rule already exists.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool Has(int index)
        {
            return PluralVariantsCustom.ContainsKey(index);
        }
        /// <summary>
        /// Get the specified plural rule.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Func<string, string[], string> Get(int index)
        {
            if (!PluralVariantsCustom.ContainsKey(index))
            {
                throw new Exception($"Cannot get custom plural form with index {index} because the index is not used.");
            }
            return PluralVariantsCustom[index];
        }

        #region Util
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetLastTwoDigits(string value)
        {
            return Int32.Parse(value.Length > 2 ? value.Substring(value.Length - 2) : value);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetLastOneDigit(string value)
        {
            return Int32.Parse(value.Length > 1 ? value.Substring(value.Length - 1) : value);
        }
        #endregion
    }
}
