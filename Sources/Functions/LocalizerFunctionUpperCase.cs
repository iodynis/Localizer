﻿using System;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerFunctionUpperCase : LocalizerFunction
    {
        /// <summary>
        /// Convert string to upper case.
        /// </summary>
        /// <param name="name">Name of the function.</param>
        public LocalizerFunctionUpperCase(string name = "UpperCase")
            : base(name)
        {
            ;
        }
        /// <summary>
        /// Convert the provided string to uppercase.
        /// </summary>
        /// <param name="language">The language. It s not used and may be <see langword="null"/>.</param>
        /// <param name="arguments">
        /// First argument is the string to modify.
        /// Second argument is the index to start and is optional and defaults to 0.
        /// Third argument is the length and is optional and defaults to the length left to the end of the string.
        /// </param>
        /// <returns>Uppercase string.</returns>
        public override object Invoke(string language, params object[] arguments)
        {
            if (arguments == null)
            {
                throw new ArgumentNullException(nameof(arguments));
            }
            if (arguments.Length == 0)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) requires at least one argument.");
            }
            if (arguments.Length > 3)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) requires three arguments at max.");
            }

            string @string = arguments[0].ToString();
            if (arguments.Length == 1)
            {
                return @string.ToUpperInvariant();
            }

            int index, length;

            // Get index
            try
            {
                index = Convert.ToInt32(arguments[1]);
            }
            catch (Exception exception)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for index {arguments[1]}. Index {arguments[1]} is not a number.", exception);
            }
            if (index <= -@string.Length || @string.Length <= index)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for index {arguments[1]}. Index modulus {Math.Abs(index)} should be lower than {@string.Length}.");
            }
            if (index < 0)
            {
                index = @string.Length - index;
            }

            if (arguments.Length == 2)
            {
                return StringToUpperCase(@string, index);
            }

            // Get length
            try
            {
                length = Convert.ToInt32(arguments[2]);
            }
            catch (Exception exception)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for length {arguments[2]}. Length {arguments[2]} is not a number.", exception);
            }
	        if (length <= 0 )
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for length {arguments[2]}. Length should be greater than zero.");
            }
	        if (index + length >= @string.Length)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for length {arguments[2]}. Length should be lower than {@string.Length - index}.");
            }
            return StringToUpperCase(@string, index, length);

        }
        private static string StringToUpperCase(string @string, int index)
        {
            int start = index < 0 ? (@string.Length - index) : index;
            if (start < 0 || start >= @string.Length)
            {
                throw new ArgumentOutOfRangeException($"Start index {index} is out of range.", nameof(index));
            }
            return @string.Substring(0, start) + @string.Substring(start).ToUpperInvariant();
        }
        private static string StringToUpperCase(string @string, int index, int length)
        {
            if (length == 0)
            {
                return @string;
            }

            int start = index < 0 ? (@string.Length - index) : index;
            if (start < 0 || start >= @string.Length)
            {
                throw new ArgumentOutOfRangeException($"Start index {index} is out of range.", nameof(index));
            }
            if (length < 0)
            {
                throw new ArgumentException("Length cannot be negative.");
            }
            if (start + length > @string.Length)
            {
                throw new ArgumentOutOfRangeException($"End index {index} is out of range.");
            }
            return @string.Substring(0, start) + @string.Substring(start, length).ToUpperInvariant() + @string.Substring(start + length);
        }
    }
}
