﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerFunction
    {
        /// <summary>
        /// By thisname the function is available in the localization text.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Localizer that the function belongs to.
        /// </summary>
        public Localizer Localizer { get; internal set; }
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="name">Name of the function.</param>
        protected LocalizerFunction(string name)
        {
            Name = name;
        }
        /// <summary>
        /// Run the function for the specified language with no arguments.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public object Invoke(string language)
        {
            return Invoke(language, new object[0]);
        }
        /// <summary>
        /// Run the function for the specified language and arguments.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns></returns>
        public object Invoke(string language, IEnumerable<object> arguments)
        {
            return Invoke(language, arguments.ToArray());
        }
        /// <summary>
        /// Run the function for the specified language and arguments.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns></returns>
        public virtual object Invoke(string language, params object[] arguments)
        {
            throw new NotImplementedException();
        }
    }
}
