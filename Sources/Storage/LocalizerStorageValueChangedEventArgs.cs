﻿using System;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerStorageValueChangedEventArgs : EventArgs
    {
        public string Key;
        public virtual object Value { get; }
        public LocalizerStorageValueChangedEventArgs(string key)
        {
            Key = key;
        }
    }
}
