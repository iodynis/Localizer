﻿using System;

namespace Iodynis.Libraries.Localizing.Storage
{
    public class LocalizerStorageDynamicValueChangedEventArgs : LocalizerStorageValueChangedEventArgs
    {
        private Func<object> _Function;
        public override object Value => _Function.Invoke();
        public LocalizerStorageDynamicValueChangedEventArgs(string key, Func<object> function)
            : base(key)
        {
            Key = key;
            _Function = function;
        }
    }
}
