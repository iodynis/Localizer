﻿namespace Iodynis.Libraries.Localizing.Storage
{
    public class LocalizerStorageStaticValueChangedEventArgs : LocalizerStorageValueChangedEventArgs
    {
        private object _Value;
        public override object Value => _Value;
        public LocalizerStorageStaticValueChangedEventArgs(string key, object value)
            : base(key)
        {
            Key = key;
            _Value = value;
        }
    }
}
