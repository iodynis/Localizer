﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Iodynis.Libraries.Localizing.Storage;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerStorage
    {
        private struct Event
        {
            public object Object;
            public string[] EventNames;
            public Action Action;
            public Event(object @object, string[] eventNames, Action action)
            {
                Object = @object;
                EventNames = eventNames;
                Action = action;
                ObjectToEventToActionToDelegate = new Dictionary<object, Dictionary<string, Dictionary<Action, Delegate>>>();
            }
            public void Subscribe()
            {
                foreach (string eventName in EventNames)
                {
                    Subscribe(Object, eventName, Action);
                }
            }
            public void Unsubscribe()
            {
                foreach (string eventName in EventNames)
                {
                    Unsubscribe(Object, eventName, Action);
                }
            }

            #region Util

            private Dictionary<object, Dictionary<string, Dictionary<Action, Delegate>>> ObjectToEventToActionToDelegate;
            private void Subscribe(object @object, string eventName, Action action)
            {
                EventInfo eventInfo = @object.GetType().GetEvent(eventName);
                Subscribe(@object, eventInfo, action);
            }
            private void Subscribe(object @object, EventInfo eventInfo, Action action)
            {
                // Prepare the event handler type
                Type eventHandlerType = eventInfo.EventHandlerType;
                MethodInfo eventHandlerInvokeMethod = eventHandlerType.GetMethod("Invoke");
                ParameterInfo[] eventHandlerInvokeMethodParameters = eventHandlerInvokeMethod.GetParameters();
                Type[] eventHandlerInvokeMethodParameterTypes = new Type[eventHandlerInvokeMethodParameters.Length];
                for (int i = 0; i < eventHandlerInvokeMethodParameters.Length; i++)
                {
                    eventHandlerInvokeMethodParameterTypes[i] = eventHandlerInvokeMethodParameters[i].ParameterType.IsByRef
                        ? eventHandlerInvokeMethodParameters[i].ParameterType.GetElementType()
                        : eventHandlerInvokeMethodParameters[i].ParameterType;
                }
                string className = "Handler";
                string methodName = eventInfo.Name + "Handler";

                // Preapare to generate IL
                AssemblyName assemblyName = new AssemblyName();
                assemblyName.Name = "DynamicEventHandler";
                AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
                ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule(assemblyName.Name);
                TypeBuilder typeBuilder = moduleBuilder.DefineType(className, TypeAttributes.Class | TypeAttributes.Public);

                //DynamicMethod dynamicMethod = new DynamicMethod();
                //dynamicMethod.

                // Build a method with arguments appropriate for the event handler and make it call the action
                //MethodBuilder methodBuilder = typeBuilder.DefineMethod(methodName, MethodAttributes.Public | MethodAttributes.Static, eventHandlerInvokeMethod.ReturnType, eventHandlerInvokeMethodParameterTypes);
                //ILGenerator il = methodBuilder.GetILGenerator();
                //LocalBuilder localBuilderAction = il.DeclareLocal(typeof(Action));

                //il.Emit(OpCodes.Ldarg, action.Method);
                //il.Emit(OpCodes.Callvirt, );
                ////il.Emit(OpCodes.Ldnull);
                //il.Emit(OpCodes.Nop);
                //il.Emit(OpCodes.Ret);

                //var mi = typeof(Action).GetMethod("Invoke");
                //il.Emit(OpCodes.Ldarg_3);
                //il.Emit(OpCodes.st);
                //il.Emit(OpCodes.Callvirt, mi);
                ////il.Emit(OpCodes.Ldnull);
                //il.Emit(OpCodes.Ret);

                //il.Emit(OpCodes.Call, typeof(Util).GetMethod("Nya"));
                //il.Emit(OpCodes.Ret);

                Type type = typeBuilder.CreateType();
                MethodInfo eventHandler = type.GetMethod(methodName);

                // Create the event handler and subscribe
                Delegate @delegate = Delegate.CreateDelegate(eventHandlerType, eventHandler);
                eventInfo.AddEventHandler(@object, @delegate);
            }
            private void Unsubscribe(object @object, string eventName, Action action)
            {
                EventInfo eventInfo = @object.GetType().GetEvent(eventName);
                Unsubscribe(@object, eventInfo, action);
            }
            private void Unsubscribe(object @object, EventInfo eventInfo, Action action)
            {
                if (!ObjectToEventToActionToDelegate.TryGetValue(@object, out Dictionary<string, Dictionary<Action, Delegate>> eventToActionToDelegate))
                {
                    return;
                }
                if (!eventToActionToDelegate.TryGetValue(eventInfo.Name, out Dictionary<Action, Delegate> actionToDelegate))
                {
                    return;
                }
                if (!actionToDelegate.TryGetValue(action, out Delegate @delegate))
                {
                    return;
                }

                // Remove event handler
                eventInfo.RemoveEventHandler(@object, @delegate);

                actionToDelegate.Remove(action);
                if (actionToDelegate.Count == 0)
                {
                    eventToActionToDelegate.Remove(eventInfo.Name);
                }
                if (eventToActionToDelegate.Count == 0)
                {
                    ObjectToEventToActionToDelegate.Remove(@object);
                }
            }

            #endregion
        }

        public event EventHandler<LocalizerStorageValueChangedEventArgs> ValueChanged;

        private Dictionary<string, object> Constants = new Dictionary<string, object>();
        private Dictionary<string, Func<object>> Variables = new Dictionary<string, Func<object>>();
        private Dictionary<string, Event> Events = new Dictionary<string, Event>();

        internal Localizer Localizer { get; }

        internal LocalizerStorage(Localizer localizer)
        {
            Localizer = localizer;
        }
        /// <summary>
        /// Check if the key is set.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>If the key is set.</returns>
        public bool Exists(string key)
        {
            return Constants.ContainsKey(key) || Variables.ContainsKey(key);
        }
        /// <summary>
        /// Retrieve the value for the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The corresponding value.</returns>
        public object Get(string key)
        {
            if (Constants.TryGetValue(key, out object constant))
            {
                return constant;
            }
            if (Variables.TryGetValue(key, out Func<object> variable))
            {
                return variable?.Invoke();
            }
            return null;
        }
        /// <summary>
        /// Retrieve the value for the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The corresponding value.</param>
        /// <returns>If the key was found.</returns>
        public bool TryGet(string key, out object value)
        {
            if (Constants.TryGetValue(key, out object constant))
            {
                value = constant;
                return true;
            }
            else if (Variables.TryGetValue(key, out Func<object> variable))
            {
                value = variable?.Invoke();
                return true;
            }
            else
            {
                value = null;
                return false;
            }
        }
        /// <summary>
        /// Set a constant value for the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The corresponding constant value.</param>
        public void Set(string key, object value)
        {
            Unset(key);
            Constants[key] = value;
            ValueChanged?.Invoke(this, new LocalizerStorageStaticValueChangedEventArgs(key, value));
        }
        /// <summary>
        /// Set a variable value for the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="function">The function to evaluate the corresponding variable value.</param>
        public void Set(string key, Func<object> function)
        {
            Unset(key);
            Variables[key] = function;
            ValueChanged?.Invoke(this, new LocalizerStorageDynamicValueChangedEventArgs(key, function));
        }
        /// <summary>
        /// Unsets the value for the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        public void Unset(string key)
        {
            if (Constants.Remove(key))
            {
                return;
            }
            if (Variables.Remove(key))
            {
                return;
            }
            if (Events.TryGetValue(key, out Event @event))
            {
                @event.Unsubscribe();
                Events.Remove(key);
                return;
            }
        }
        /// <summary>
        /// Set a property value for the specified key.
        /// The value will be updated every time the event is fired.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="object">The object that has the property.</param>
        /// <param name="propertyName">The property name.</param>
        /// <param name="eventNames">The event names on which to update the value.</param>
        public void Set(string key, object @object, string propertyName, params string[] eventNames)
        {
            Unset(key);

            PropertyInfo propertyInfo = @object.GetType().GetProperty(propertyName);
            Action action = () => Set(key, propertyInfo.GetValue(@object));
            Event @event = new Event(@object, eventNames, action);

            @event.Subscribe();
            Events.Add(key, @event);
        }
        ///// <summary>
        ///// Set a property value for the specified key.
        ///// The value will be updated every time the event is fired.
        ///// </summary>
        ///// <param name="key">The key.</param>
        ///// <param name="object"></param>
        ///// <param name="propertyName">The property name.</param>
        ///// <param name="eventName">The event on which to update the value.</param>
        //public void Register(string key, object @object, string propertyName, string eventName)
        //{
        //    PropertyInfo propertyInfo = @object.GetType().GetProperty(propertyName);

        //    @object.Subscribe(eventName, () =>
        //    {
        //        Set(key, propertyInfo.GetValue(@object));
        //    });
        //}

        //else if (EventInfo.EventHandlerType.GenericTypeArguments.Length == 1)
        //{
        //    if (!EventInfo.EventHandlerType.GenericTypeArguments[0].IsValueType)
        //    {
        //        // Convert (object, object) delegate to whatever event handler wants
        //        Type[] types = new [] { typeof(object), typeof(object) };
        //        MethodInfo methodInfo = GetType().GetMethod(nameof(EventPropertyValueChangedHandlerObject), BindingFlags.NonPublic | BindingFlags.Instance, null, EventPropertyValueChangedMethodArguments, null);
        //    }
        //    else
        //    {
        //        // Convert (object, T) delegate to whatever event handler wants
        //        EventPropertyValueChangedMethodArguments = new [] { typeof(object), EventInfo.EventHandlerType.GenericTypeArguments[0] };
        //        // Get the type that we want in put into generic method
        //        Type valueType = EventInfo.EventHandlerType.GenericTypeArguments[0];
        //        // Get generic method
        //        MethodInfo propertyValueChangedMethodInfoGeneric = GetType().GetMethod(nameof(EventPropertyValueChangedHandlerGeneric), BindingFlags.NonPublic | BindingFlags.Instance, null, EventPropertyValueChangedMethodArguments, null);
        //        //MethodInfo propertyValueChangedMethodInfoGeneric = GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).First(_methodInfo =>
        //        //{
        //        //    return _methodInfo.IsGenericMethod && _methodInfo.Name == nameof(EventPropertyValueChangedHandlerGeneric);
        //        //});
        //        // Make specific method
        //        EventPropertyValueChangedMethodInfo = propertyValueChangedMethodInfoGeneric.MakeGenericMethod(new [] { valueType });
        //    }
        //}
        //else
        //{
        //    throw new Exception($"Event {EventName} is not of a supported type.");
        //}
        //// Create delegate with appropriate signature
        //EventHandler eventhandler = Delegate.CreateDelegate(eventInfo.EventHandlerType, this, EventPropertyValueChangedMethodInfo);
    }
}
